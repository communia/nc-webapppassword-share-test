import base64 from 'base-64';
const config = require("./credentials.json");

const username = config.username
const password = config.password
const url = config.url
const authHeader = 'Basic ' + base64.encode(username + ':' + password)
const dav_path = '/remote.php/dav/files/' + username
const share_path = '/index.php/apps/webapppassword/api/v1/shares'
const propertyRequestBody = `<?xml version="1.0"?>
<d:propfind  xmlns:d="DAV:" xmlns:oc="http://owncloud.org/ns" xmlns:nc="http://nextcloud.org/ns">
  <d:prop>
        <d:getlastmodified />
        <d:getetag />
        <d:getcontenttype />
        <d:resourcetype />
        <oc:fileid />
        <oc:permissions />
        <oc:size />
        <d:getcontentlength />
        <nc:has-preview />
        <oc:favorite />
        <oc:comments-unread />
        <oc:owner-display-name />
        <oc:share-types />
  </d:prop>
</d:propfind>`

// TEST CASE 1: Perform dav call (must pass if using webapppassword)
const element = document.createElement('div');
const directoryContent = getDirectoryContents().then ( response => {
		//element.innerHTML = response.data.quote;
	    console.debug(response)
		element.innerHTML = '<h1>dav test</h1><textarea>' + response.text + '</textarea>';
		document.body.appendChild(element);
})

// TEST CASE 2: Create a share with existing file (if created in your nextcloud must pass)
const share_element = document.createElement('div');
const shareContent = createShare('/test_shared.txt').then ( response => {
		//element.innerHTML = response.data.quote;
	    console.debug(response)
		share_element.innerHTML = '<h1>share api test</h1><textarea>' + JSON.stringify(response.text) + '</textarea>';
		document.body.appendChild(share_element);
}, error => {
	console.debug(error)
	share_element.innerHTML = '<h1>share api test unexisting path ERROR</h1><textarea>Network error means cors failing, but may be some other issue, look at browser webdevtools log for message of the failure, as this will not be available to this script: ' + JSON.stringify(error) + '</textarea>'
	document.body.appendChild(share_element);
})

// TEST CASE 3: Try to create a share with non existing file (will not pass if not existing)
const unexisting_share_element = document.createElement('div');
const unexisting_shareContent = createShare('/dj929fv9.txt').then ( response => {
	    console.debug(response)
		unexisting_share_element.innerHTML = '<h1>share api test - unexisting path</h1><textarea>' + JSON.stringify(response.text) + '</textarea>';
		document.body.appendChild(unexisting_share_element);
}, error => {
	console.error(error)
	unexisting_share_element.innerHTML = '<h1>share api test unexisting path ERROR</h1><textarea>Network error means cors failing, but may be some other issue, look at browser webdevtools log for message of the failure, as this will not be available to this script: ' + JSON.stringify(error) + '</textarea>';
	document.body.appendChild(unexisting_share_element);
})

// TEST CASE 4: USE the native files_sharing api (will not pass if not tweaking server or nextcloud files_sharing code)
const native_share_element = document.createElement('div');
const native_shareContent = createShare('/test_shared.txt', true).then ( response => {
		//element.innerHTML = response.data.quote;
	    console.debug(response)
		native_share_element.innerHTML = '<h1>Native share api test</h1><textarea>' + JSON.stringify(response.text) + '</textarea>';
		document.body.appendChild(native_share_element);
}, error => {
	console.debug(error)
	native_share_element.innerHTML = '<h1>Native share api testERROR</h1><textarea>Network error means cors failing, but may be some other issue, look at browser webdevtools log for message of the failure, as this will not be available to this script: ' + JSON.stringify(error) + '</textarea>'
	document.body.appendChild(native_share_element);
})

function getDirectoryContents(path) {
	const headers = new Headers()
	headers.append('Accept', 'text/plain')
	headers.append('Depth', '1')
	headers.append('Content-Type', 'application/xml')
	headers.append('Authorization', authHeader)

	return new Promise((resolve, reject) => {
			fetch(url + dav_path, {
				method: 'PROPFIND',
				credentials: 'omit',
				headers,
				body: propertyRequestBody,
			}).then((response) => {
				response.text().then(text => {
					if (response.status < 400) {
						resolve({text, path})
					} else {
						reject(new Error({ response }), text)
					}
				})
			}).catch(err => {
				console.error(err)
				reject(err)
			})
		})
}

function createShare(path, native = false) {
	const headers = new Headers()
	headers.append('Accept', 'application/json')
	headers.append('OCS-APIRequest', 'true')
	headers.append('Content-Type', 'application/json')
	headers.append('Authorization', authHeader)
	const req = {
		path: path,
		shareType: 3,
		label: 'test_shared native: ' + native,
		expireDate: null,
		password: undefined,
	}

	return new Promise((resolve, reject) => {
		    let endpoint = (native)? '/ocs/v2.php/apps/files_sharing/api/v1/shares' : share_path;
			fetch(url + endpoint, {
				method: 'POST',
				credentials: 'omit',
				headers,
				body: JSON.stringify(req),
			}).then((response) => {
				response.json().then(text => {
					console.debug(text)
					if (response.status < 400) {
						resolve({text, path})
					} else {
						reject(new Error({ response }), text)
					}
				})
			}).catch(err => {
				console.error(err)
				reject(err)
			})
		})
}
