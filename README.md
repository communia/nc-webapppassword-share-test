# Webapppassord manual test case

Clone this repo. And inside project dir:

`npm install`

edit src/credentials.json to fit your needs.

run `npx webpack`

Allow the used origin in webapppassword settings share and dav fields.

Add a file called test_shared.txt in nextcloud used user files root.

Use dist directory contents somewhere from the domain allowed in webapppassword settings, for example:
`https://allowed-domain-in-webapppasword-settings.com/webapppassword-share-test/dist/index.html`

# Tests executed

Tests will appear as soon as are ran, and will fill a text area with the response or explaining what it can based on the error:

- PASS TEST CASE 1: Perform dav call (must pass if using webapppassword). Dav test, success expected as this is the main webapppassword use, and is largely tested if origin is allowed.
- PASS TEST CASE 2: Create a share with existing file (if created in your nextcloud must pass). Share api test, success expected if /test_shared.txt exists in nextcloud user files root dir, and origin is allowed. You could check in nextcloud if a share of this file is created with label `test_shared native: false`
- ERROR TEST CASE 3: Try to create a share with non existing file (will not pass if not existing).
- ERROR TEST CASE 4: USE the native files_sharing api (will not pass if not tweaking server or nextcloud files_sharing code). Native share api test , expected a CORS error in POST return, as native app files_sharing is not allowed to be called in remote origin.
